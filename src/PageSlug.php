<?php


namespace Eropadev\Content;


use Eropadev\Content\Models\Lang;
use Eropadev\Content\Models\PageLang;
use Eropadev\Content\Models\PageLangBlock;
use Eropadev\Content\Services\ContentRead;

/**
 * Class PageSlug
 * @package Eropadev\Content
 *  получаем данные по slug
 */
class PageSlug
{
    /**
     * получаем все данные по slug
     * @param $slug
     * @return mixed
     */
    static function getPageslug($slug){
        $data=PageLang::where('slug',$slug)->first();
        if(is_null($data)){
            abort(404);
        }
        return $data;
    }


    /**
     * Получаем параметры страницы
     * @param $lang
     * @param $slug
     * @return mixed
     */
    static function getPageslugLang($lang,$slug){

        $langData=Lang::where('short_name',$lang)->first();

        if(is_null($langData))
            abort(404);

        $data=PageLang::where('slug',$slug)->where('lang_id',$langData->id)->first();

        if(is_null($data))
            abort(404);

        return $data;
    }

    static function getDataBlockId($idblock,$page_lang){
        $data=PageLangBlock::where('block_id',$idblock)
            ->where('page_lang_id',$page_lang)
            ->first();
        if(is_null($data))
            return "";
        $readContent=new ContentRead($data->html_value);
        $d=$readContent->readContemt();
        return $d;
    }
}
