<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Пример веб-страницы</title>
        <link rel="stylesheet" href="{{ asset('/eropadev/content/back/css/main.css') }}">
        <script src="https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
    </head>
    <body>


    <div class="wrapper_body">
        <div class="left_part">
            <div class="logo">
                <div class="logtext" >
                    <img src="{{ asset('/eropadev/content/back/png/logoalsodev.png') }}" width="54" alt="logo">
                    <div style="margin-top:22px;">
                        AlsoDev / Content
                    </div>

                </div>
            </div>
            <div class="menu_des">
                <lu class="menu_lu_desk">
                    <li class="lu_menu_link">
                        <a href="{{ route('back.crud.index',["param" => "home"]) }}" class="menu_link">
                            Главная страница
                        </a>
                    </li>
                    <li  class="lu_menu_link">
                        <a href="{{ route('back.crud.index',["param" => "lang"]) }}" class="menu_link">
                            Языки
                        </a>
                    </li>
                    <li  class="lu_menu_link">
                        <a href="{{ route('back.crud.index',["param" => "block"]) }}" class="menu_link">
                            Блоки
                        </a>
                    </li>
                    <li  class="lu_menu_link">
                        <a href="{{ route('back.crud.index',["param" => "page"]) }}" class="menu_link">
                            Страницы
                        </a>
                    </li>
                    <li  class="lu_menu_link">
                        <a href="{{ route('back.crud.index',["param" => "blogpage"]) }}" class="menu_link">
                            Блог страниы
                        </a>
                    </li>
                    <li  class="lu_menu_link">
                        <a href="#" class="menu_link">
                            Выxод
                        </a>
                    </li>
                </lu>
            </div>

        </div>

        <div class="right_part">
            @yield('content')
        </div>

    </div>

    @yield('myjs')

    </body>
</html>
