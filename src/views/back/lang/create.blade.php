@extends('myviews::back.app')


@section('content')





    <div class="title_div">
        Добавить новую запись языка
    </div>

    <div class="hleb_krohka">
        <a href="{{ route('back.crud.index',["param" => "home"]) }}">Главная страница</a> /
        <a href="{{ route('back.crud.index',["param" => "lang"]) }}">Языки</a>  /
        Новая запись
    </div>

   <div >
       <form  class="crud_form" action="{{ route('back.crud.store') }}" method="post">
           <div class="row_line">
                <span>Полное название</span>
                <input class="form_input" name="short_name" >
           </div>
           <div class="row_line">
               <span>Полное название</span>
               <input class="form_input" name="full_name" >
           </div>
           <div class="row_line">
               <input type="submit" class="form_submit" value="Создать">
           </div>
           @csrf
           <input type="hidden" name="param" value="lang">
       </form>
   </div>
@endsection

@section('myjs')


@endsection

