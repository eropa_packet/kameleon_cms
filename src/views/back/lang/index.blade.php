@extends('myviews::back.app')


@section('content')





    <div class="title_div">
        Языки
    </div>
    <div class="hleb_krohka">
        <a href="{{ route('back.crud.index',["param" => "home"]) }}">Главная страница</a> /
        Языки
    </div>
    <div class="list_btn_crud">
        <a href="{{ route('back.crud.create',["param" => "lang"]) }}">Добавить запись</a>
    </div>
    <div class="crud_table">

        <table class="customers">
            <tr>
                <th>id</th>
                <th>Название</th>
                <th>Действия</th>
            </tr>
            @foreach($datas as $data)
                <tr>
                    <td>{{$data->id}}</td>
                    <td>{{$data->short_name}}</td>
                    <td>
                        <a href="{{ route('back.crud.edit',["param" => "lang","id"=>$data->id]) }}" class="btn_edit">edit</a>
                        <a href="{{ route('back.crud.delete',["param" => "lang","id"=>$data->id]) }}" class="btn_delete">delete</a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection

@section('myjs')
@endsection

