<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Пример веб-страницы</title>
    <link rel="stylesheet" href="{{ asset('/back/main.css') }}">

    <style>

        html{
            box-sizing: border-box;
        }
        *,*::before,*::after{
            box-sizing: inherit;
            margin: 0px;
        }
        .head_wrapper{
            display: flex;
            flex-wrap: wrap;
            background: #cbd5e0;
            height: 50px;
            justify-content: space-between;
        }
        .body_wrapper{
            display: flex;
            flex-wrap: wrap;
            min-height: calc(100vh - 100px);
        }
        .menu_des{
           min-width: 250px;
            background: #e2e8f0;
        }

        .footer{
            height: 50px;
            background: #4a5568;
            color: white;
            text-align: center;
        }

    </style>

</head>
<body>


    <div class="head_wrapper">
        <div class="leftPart">
            My Content (v 1.0)
        </div>
        <div class="rightPart">
            Выйти
        </div>
    </div>

    <div class="body_wrapper">
        <div class="menu_des">
                <lu>
                    <li>
                        Язык
                    </li>
                    <li>
                        Блоки
                    </li>
                    <li>
                        Страницы
                    </li>
                </lu>
        </div>
        <div class="info_content">
            тут остольное
        </div>
    </div>
    <div class="footer">
        <p>
            тут подвал
        </p>

    </div>






</body>
</html>
