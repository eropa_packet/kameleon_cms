@extends('myviews::back.app')


@section('content')

    <div class="title_div">
        Блог на сайте
    </div>
    <div class="hleb_krohka">
        <a href="{{ route('back.crud.index',["param" => "home"]) }}">Главная страница</a> /
        Блог на сайте
    </div>
    <div class="list_btn_crud">
        <a href="{{ route('back.crud.create',["param" => "blogpage"]) }}">Добавить запись</a>
    </div>
    <div class="crud_table">

        <table class="customers">
            <tr>
                <th>id</th>
                <th>Дата публикаии</th>
                <th>Название</th>
                <th>link (slug) </th>
                <th>Действия</th>
            </tr>
            @foreach($datas as $data)
                <tr>
                    <td>{{$data->id}}</td>
                    <td>{{$data->datepublic}}</td>
                    <td>{{$data->title}}</td>
                    <td>{{$data->slug}}</td>
                    <td>
                        <a href="{{ route('back.crud.edit',["param" => "blogpage","id"=>$data->id]) }}" class="btn_edit">edit</a>
                        <a href="{{ route('back.crud.delete',["param" => "blogpage","id"=>$data->id]) }}" class="btn_delete">delete</a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection

@section('myjs')
@endsection

