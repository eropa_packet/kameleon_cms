@extends('myviews::back.app')


@section('content')

    <div class="title_div">
       Редактировать блог страниы
    </div>

    <div class="hleb_krohka">
        <a href="{{ route('back.crud.index',["param" => "home"]) }}">Главная страница</a> /
        <a href="{{ route('back.crud.index',["param" => "blogpage"]) }}">Блог на странице</a>  /
        Редактировать запись
    </div>

   <div >
       <form  class="crud_form" action="{{ route('back.crud.update') }}"    enctype="multipart/form-data" method="post">
           <div class="row_line">
               <span>Картинка ПК</span>
               <input class="form_input"  type="file" name="img_pc" >
               <img src="{{asset($data->img_pc)}}" width="300">
           </div>

           <div class="row_line">
               <span>Картинка Моб</span>
               <input class="form_input"  type="file" name="img_mobi" >
               <img src="{{asset($data->img_mobi)}}" width="300">
           </div>
           <div class="row_line">
               <span>OG Image</span>
               <input class="form_input" name="ogimage"  type="file" >
               <img src="{{asset($data->ogimage)}}" width="300">
           </div>
           <div class="row_line">
               <span>OG Title</span>
               <input class="form_input" name="ogtitle"  value="{{$data->ogtitle}}">

           </div>
           <div class="row_line">
               <span>OG Description</span>
               <textarea name="ogdescriptor" rows="5" cols="33" style="padding: 10px;
                    max-width: 100%;
                    line-height: 1.5;
                    border-radius: 5px;
                    border: 1px solid #ccc;
                    width: 70%;
                    box-shadow: 1px 1px 1px #999;">{{$data->ogdescriptor}}</textarea>
           </div>
           <div class="row_line">
               <span>Заголовок</span>
               <input class="form_input" name="title"  value="{{$data->title}}">
           </div>
           <div class="row_line">
               <span>Показывать картинку </span>
               <select name="showimage">
                   <option value="1" @if($data->showimage==1)selected @endif>ДА</option>
                   <option value="0" @if($data->showimage==0)selected @endif>НЕТ</option>
               </select>
           </div>
           <div class="row_line">
               <span>Дата публикации</span>
               <input class="form_input" type="datetime-local" name="datepublic"
                      value="{{ \Carbon\Carbon::parse($data->datepublic)->format('Y-m-d\TH:i')}}" >
           </div>
           <div class="row_line">
               <span>link (slug)</span>
               <input class="form_input" name="slug" value="{{$data->slug}}">
           </div>
           <div class="row_line">
               <span>Share link</span>
               <input class="form_input" type="number" value="{{$data->share}}" name="share" >
           </div>

           <div class="row_line">
               <span>Look link</span>
               <input class="form_input" type="number" value="{{$data->look}}" name="look" >
           </div>
           <div class="row_line">
               <span>Маленькое описание</span>
               <textarea name="smtext" rows="5" cols="33" style="padding: 10px;
                    max-width: 100%;
                    line-height: 1.5;
                    border-radius: 5px;
                    border: 1px solid #ccc;
                    width: 70%;
                    box-shadow: 1px 1px 1px #999;">{{$data->smtext}}</textarea>
           </div>
           <div class="row_line">
               <span>Текст статьи</span>
               <textarea name="fulltext"  class="fulltext" rows="25" cols="33" style="padding: 10px;
                    max-width: 100%;
                    line-height: 1.5;
                    border-radius: 5px;
                    border: 1px solid #ccc;
                    width: 90%;
                    box-shadow: 1px 1px 1px #999;">{{$data->fulltext}}</textarea>
           </div>
           <div class="row_line">
               <span>Выбрите язык</span>
               <select name="lang_id">
                   @foreach($langs as $langiem)
                       <option value="{{$langiem->id}}"

                            @if($data->lang_id==$langiem->id) selected @endif
                            >{{$langiem->short_name}}</option>
                   @endforeach
               </select>
           </div>
           <div class="row_line">
               <input type="submit" class="form_submit" value="Создать">
           </div>
           @csrf
           <input type="hidden" name="param" value="blogpage">
           <input type="hidden" name="id_record" value="{{$data->id}}">
           @csrf
       </form>
   </div>
@endsection

@section('myjs')

    <script>
        CKEDITOR.replace( 'fulltext', {
            allowedContent : true,
            filebrowserUploadUrl: "{{route('uploadimage', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        } );
    </script>

@endsection

