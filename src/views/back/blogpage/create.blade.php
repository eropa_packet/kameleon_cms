@extends('myviews::back.app')





@section('content')





    <div class="title_div">
        Добавить новый блог страницы
    </div>

    <div class="hleb_krohka">
        <a href="{{ route('back.crud.index',["param" => "home"]) }}">Главная страница</a> /
        <a href="{{ route('back.crud.index',["param" => "blogpage"]) }}">  Блоки на странице</a>  /
        Новая запись
    </div>

   <div >
       <form  class="crud_form"
              action="{{ route('back.crud.store') }}"
              enctype="multipart/form-data"
              method="post">
           <div class="row_line">
                <span>Картинка ПК</span>
                <input class="form_input"  type="file" name="img_pc"  required>
           </div>

           <div class="row_line">
               <span>Картинка Моб</span>
               <input class="form_input"  type="file" name="img_mobi"  >
           </div>
           <div class="row_line">
               <span>OG Image</span>
               <input class="form_input" name="ogimage"  type="file"  >
           </div>
           <div class="row_line">
               <span>OG Title</span>
               <input class="form_input" name="ogtitle"  >
           </div>
           <div class="row_line">
               <span>OG Description</span>
               <textarea name="ogdescriptor" rows="5" cols="33" style="padding: 10px;
                    max-width: 100%;
                    line-height: 1.5;
                    border-radius: 5px;
                    border: 1px solid #ccc;
                    width: 70%;
                    box-shadow: 1px 1px 1px #999;" >
               </textarea>
           </div>
           <div class="row_line">
               <span>Заголовок</span>
               <input class="form_input" name="title"  required>
           </div>
           <div class="row_line">
               <span>Показывать картинку </span>
               <select name="showimage">
                   <option value="1" >ДА</option>
                   <option value="0" selected>НЕТ</option>
               </select>
           </div>
           <div class="row_line">
               <span>Дата публикации</span>
               <input class="form_input" type="datetime-local" name="datepublic"  >
           </div>
           <div class="row_line">
               <span>link (slug)</span>
               <input class="form_input" name="slug" >
           </div>

           <div class="row_line">
               <span>Share link</span>
               <input class="form_input" type="number" value="0" name="share" >
           </div>

           <div class="row_line">
               <span>Look link</span>
               <input class="form_input" type="number" value="0" name="look" >
           </div>

           <div class="row_line">
               <span>Маленькое описание</span>
               <textarea name="smtext" rows="5" cols="33" style="padding: 10px;
                    max-width: 100%;
                    line-height: 1.5;
                    border-radius: 5px;
                    border: 1px solid #ccc;
                    width: 70%;
                    box-shadow: 1px 1px 1px #999;" >
               </textarea>
           </div>
           <div class="row_line">
               <span>Текст статьи</span>
               <textarea name="fulltext"  class="fulltext"  rows="25" cols="33" style="padding: 10px;
                    max-width: 100%;
                    line-height: 1.5;
                    border-radius: 5px;
                    border: 1px solid #ccc;
                    width: 90%;
                    box-shadow: 1px 1px 1px #999;" >
               </textarea>
           </div>
           <div class="row_line">
               <span>Выбрите язык</span>
               <select name="lang_id" required>
                   @foreach($langs as $langiem)
                       <option value="{{$langiem->id}}">{{$langiem->short_name}}</option>
                   @endforeach
               </select>
           </div>
           <div class="row_line">
               <input type="submit" class="form_submit" value="Создать">
           </div>
           @csrf
           <input type="hidden" name="param" value="blogpage">
       </form>
   </div>
@endsection

@section('myjs')
    <script>
        CKEDITOR.replace( 'fulltext', {
            allowedContent : true,
            filebrowserUploadUrl: "{{route('uploadimage', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        } );
    </script>
@endsection

