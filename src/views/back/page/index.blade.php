@extends('myviews::back.app')


@section('content')

    <div class="title_div">
        Страницы
    </div>
    <div class="hleb_krohka">
        <a href="{{ route('back.crud.index',["param" => "home"]) }}">Главная страница</a> /
        Страницы
    </div>
    <div class="list_btn_crud">
        <a href="{{ route('back.crud.create',["param" => "page"]) }}">Добавить запись</a>
    </div>
    <div class="crud_table">

        <table class="customers">
            <tr>
                <th>id</th>
                <th>Название</th>
                <th>Комментарий</th>
                <th>Действия</th>
            </tr>

            @foreach($datas as $data)
                <tr>
                    <td>{{$data->id}}</td>
                    <td>{{$data->name}}</td>
                    <td>{{$data->comment}}</td>
                    <td>
                        <a href="{{ route('back.crud.edit',["param" => "page","id"=>$data->id]) }}" class="btn_edit">edit</a>
                        <a href="{{ route('back.crud.delete',["param" => "page","id"=>$data->id]) }}" class="btn_delete">delete</a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection

@section('myjs')
@endsection

