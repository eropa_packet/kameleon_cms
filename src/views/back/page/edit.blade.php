@extends('myviews::back.app')


@section('content')

    <div class="title_div">
        Редактировать страницу
    </div>

    <div class="hleb_krohka">
        <a href="{{ route('back.crud.index',["param" => "home"]) }}">Главная страница</a> /
        <a href="{{ route('back.crud.index',["param" => "page"]) }}">Страницы</a>  /
        Редактировать запись
    </div>

   <div>
       <form  class="crud_form" action="{{ route('back.crud.update') }}" method="post">
           <div class="row_line">
                <span>Название</span>
                <input class="form_input" name="name" value="{{$data->name}}" >
           </div>
           <div class="row_line">
               <span>Комментарий</span>
               <input class="form_input" name="comment" value="{{$data->comment}}">
           </div>
           <div class="row_line">
               <input type="submit" class="form_submit" value="Редактировать">
           </div>
           <input type="hidden" id="id_page" name="id_record" value="{{$data->id}}">
           <input type="hidden" name="param" value="page">
           @csrf
       </form>
   </div>
    <div  class="crud_form">
        <h2>Язык</h2>
        <?php
            $langs=\Eropadev\Content\Models\Lang::all();
        ?>
        <select id="selectLang" >
            <option value="" selected disabled>Выберите язык</option>
            @foreach($langs as $lang)
                <option value="{{$lang->id}}">{{$lang->short_name}}</option>
            @endforeach
        </select>
        <div class="div_ajax">
            <div class="cssload-thecube">
                <div class="cssload-cube cssload-c1"></div>
                <div class="cssload-cube cssload-c2"></div>
                <div class="cssload-cube cssload-c4"></div>
                <div class="cssload-cube cssload-c3"></div>
            </div>
        </div>
        <div class="return_ajax" style="width: 100%;">
        </div>

    </div>
@endsection

@section('myjs')
    <script
        src="https://code.jquery.com/jquery-3.6.0.slim.min.js"
        integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI="
        crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script>
        $('#selectLang').on('change',function () {
            let lang=$(this).val();
            let page_id=$("#id_page").val();
            if(lang=="")
                return 1;
            $('.return_ajax').html("");
            $('.cssload-thecube').show();
            $.ajax(
                {
                    type: 'POST',
                    url: "{{ route("back.crud.ajax_page")}}",
                    data: {
                        "lang_id": lang,
                        "page_id": page_id
                    },
                    success: function(result){
                        //$("#div1").html(result);
                        $('.cssload-thecube').hide();
                        $('.return_ajax').html(result);

                    }
                }
            );
        });
        $(document).on('click','#pageSave',function () {
            let formSlug=$('#formSlug').val();
            let formMetatitle=$('#formMetatitle').val();
            let formMetadescriptor=$('#formMetadescriptor').val();
            let formHtmlValue=$('#formHtmlValue').val();
            let selectLang=$('#selectLang').val();
            let id_page=$('#id_page').val();
            var block=[] ;
            $( ".htmkblock" ).each(function() {
                let nameBlock=$(this).data('nameblock');
                let valHtml=$(this).val();
                block.push({ nameblock: nameBlock, htmlvalue:valHtml, })
            });

            $.ajax(
                {
                    type: 'POST',
                    url: "{{ route("back.crud.ajax_pageupdate")}}",
                    data: {
                        "lang_id": selectLang,
                        "id_page":id_page,
                        "formSlug":formSlug,
                        "formMetatitle":formMetatitle,
                        "formMetadescriptor":formMetadescriptor,
                        "formHtmlValue":formHtmlValue,
                        "block":block,
                    },
                    success: function(result){
                        //$("#div1").html(result);
                      alert('Изменения были cоxранены');

                    }
                }
            );
        })
        $(document).on('change','#addBlockData',function () {
           let selectVal=$(this).val();
           let countBloc=$('textarea').hasClass('block_id_'+selectVal);
           if(countBloc){
               alert('Такой блок уже есть');
               return;
           }
           let htmlAddBloc='<br>Block #'+selectVal+'- <textarea ' +
               ' data-nameblock="'+selectVal+'"' +
               'class="block_id_'+selectVal+' htmkblock"></textarea>';
           var data=document.getElementById('div_block_datas').innerHTML+=htmlAddBloc;
        })
    </script>


    <style>
        .div_ajax{
            width: 100%;
        }
        .cssload-thecube {
            width: 73px;
            height: 73px;
            margin: 0 auto;
            margin-top: 49px;
            position: relative;
            transform: rotateZ(45deg);
            -o-transform: rotateZ(45deg);
            -ms-transform: rotateZ(45deg);
            -webkit-transform: rotateZ(45deg);
            -moz-transform: rotateZ(45deg);
            display: none;
        }
        .cssload-thecube .cssload-cube {
            position: relative;
            transform: rotateZ(45deg);
            -o-transform: rotateZ(45deg);
            -ms-transform: rotateZ(45deg);
            -webkit-transform: rotateZ(45deg);
            -moz-transform: rotateZ(45deg);
        }
        .cssload-thecube .cssload-cube {
            float: left;
            width: 50%;
            height: 50%;
            position: relative;
            transform: scale(1.1);
            -o-transform: scale(1.1);
            -ms-transform: scale(1.1);
            -webkit-transform: scale(1.1);
            -moz-transform: scale(1.1);
        }
        .cssload-thecube .cssload-cube:before {
            content: "";
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color:  #e3454a;
            animation: cssload-fold-thecube 2.76s infinite linear both;
            -o-animation: cssload-fold-thecube 2.76s infinite linear both;
            -ms-animation: cssload-fold-thecube 2.76s infinite linear both;
            -webkit-animation: cssload-fold-thecube 2.76s infinite linear both;
            -moz-animation: cssload-fold-thecube 2.76s infinite linear both;
            transform-origin: 100% 100%;
            -o-transform-origin: 100% 100%;
            -ms-transform-origin: 100% 100%;
            -webkit-transform-origin: 100% 100%;
            -moz-transform-origin: 100% 100%;
        }
        .cssload-thecube .cssload-c2 {
            transform: scale(1.1) rotateZ(90deg);
            -o-transform: scale(1.1) rotateZ(90deg);
            -ms-transform: scale(1.1) rotateZ(90deg);
            -webkit-transform: scale(1.1) rotateZ(90deg);
            -moz-transform: scale(1.1) rotateZ(90deg);
        }
        .cssload-thecube .cssload-c3 {
            transform: scale(1.1) rotateZ(180deg);
            -o-transform: scale(1.1) rotateZ(180deg);
            -ms-transform: scale(1.1) rotateZ(180deg);
            -webkit-transform: scale(1.1) rotateZ(180deg);
            -moz-transform: scale(1.1) rotateZ(180deg);
        }
        .cssload-thecube .cssload-c4 {
            transform: scale(1.1) rotateZ(270deg);
            -o-transform: scale(1.1) rotateZ(270deg);
            -ms-transform: scale(1.1) rotateZ(270deg);
            -webkit-transform: scale(1.1) rotateZ(270deg);
            -moz-transform: scale(1.1) rotateZ(270deg);
        }
        .cssload-thecube .cssload-c2:before {
            animation-delay: 0.35s;
            -o-animation-delay: 0.35s;
            -ms-animation-delay: 0.35s;
            -webkit-animation-delay: 0.35s;
            -moz-animation-delay: 0.35s;
        }
        .cssload-thecube .cssload-c3:before {
            animation-delay: 0.69s;
            -o-animation-delay: 0.69s;
            -ms-animation-delay: 0.69s;
            -webkit-animation-delay: 0.69s;
            -moz-animation-delay: 0.69s;
        }
        .cssload-thecube .cssload-c4:before {
            animation-delay: 1.04s;
            -o-animation-delay: 1.04s;
            -ms-animation-delay: 1.04s;
            -webkit-animation-delay: 1.04s;
            -moz-animation-delay: 1.04s;
        }



        @keyframes cssload-fold-thecube {
            0%, 10% {
                transform: perspective(136px) rotateX(-180deg);
                opacity: 0;
            }
            25%,
            75% {
                transform: perspective(136px) rotateX(0deg);
                opacity: 1;
            }
            90%,
            100% {
                transform: perspective(136px) rotateY(180deg);
                opacity: 0;
            }
        }

        @-o-keyframes cssload-fold-thecube {
            0%, 10% {
                -o-transform: perspective(136px) rotateX(-180deg);
                opacity: 0;
            }
            25%,
            75% {
                -o-transform: perspective(136px) rotateX(0deg);
                opacity: 1;
            }
            90%,
            100% {
                -o-transform: perspective(136px) rotateY(180deg);
                opacity: 0;
            }
        }

        @-ms-keyframes cssload-fold-thecube {
            0%, 10% {
                -ms-transform: perspective(136px) rotateX(-180deg);
                opacity: 0;
            }
            25%,
            75% {
                -ms-transform: perspective(136px) rotateX(0deg);
                opacity: 1;
            }
            90%,
            100% {
                -ms-transform: perspective(136px) rotateY(180deg);
                opacity: 0;
            }
        }

        @-webkit-keyframes cssload-fold-thecube {
            0%, 10% {
                -webkit-transform: perspective(136px) rotateX(-180deg);
                opacity: 0;
            }
            25%,
            75% {
                -webkit-transform: perspective(136px) rotateX(0deg);
                opacity: 1;
            }
            90%,
            100% {
                -webkit-transform: perspective(136px) rotateY(180deg);
                opacity: 0;
            }
        }

        @-moz-keyframes cssload-fold-thecube {
            0%, 10% {
                -moz-transform: perspective(136px) rotateX(-180deg);
                opacity: 0;
            }
            25%,
            75% {
                -moz-transform: perspective(136px) rotateX(0deg);
                opacity: 1;
            }
            90%,
            100% {
                -moz-transform: perspective(136px) rotateY(180deg);
                opacity: 0;
            }
        }
    </style>
@endsection

