<div class="ajax">

    <div class="row_line">
        <span>ЧПУ(должно быть уникальное, иначе выбериться первое значение в БД)</span>
        <input class="form_input"
               name="slug"
               id="formSlug"
               value="{{ (is_null($dataLangPage)?"":$dataLangPage->slug)}}"
        >
    </div>

    <div class="row_line">
        <span>Meta title</span>
        <input class="form_input"
               name="metatitle"
               id="formMetatitle"
               value="{{ (is_null($dataLangPage)?"":$dataLangPage->metatitle)}}"
        >
    </div>

    <div class="row_line">
        <span>Meta descriptor</span>
        <input class="form_input"
               name="metadescriptor"
               id="formMetadescriptor"
               value="{{ (is_null($dataLangPage)?"":$dataLangPage->metadescriptor)}}"
        >
    </div>

    <div class="row_line">
        <span>Код страницы</span>
        <textarea rows="4" cols="50" id="formHtmlValue" name="html_value">{{(is_null($dataLangPage)?"":$dataLangPage->html_value)}}</textarea>
    </div>

    <hr>

    <?php
        $dataBlocks=\Eropadev\Content\Models\Block::all();
        if(!is_null($dataLangPage))
            $dataBlockPages=\Eropadev\Content\Models\PageLangBlock::where('page_lang_id',$dataLangPage->id)->get();
    ?>

    Блоки

    <select id="addBlockData">
        <option value="0" selected disabled>Выберите блок</option>
        @foreach($dataBlocks as $dataBlock)
            <option value="{{$dataBlock->name}}">{{$dataBlock->name}}</option>
        @endforeach
    </select>

    <hr>

    <div id="div_block_datas">
        @if(!is_null($dataLangPage))
            @foreach($dataBlockPages as $dataBlockPage)
                <?php

                    $dbBlock=\Eropadev\Content\Models\Block::query()->where('id',$dataBlockPage->block_id)->first();
                ?>
                @if(!is_null($dbBlock))
                    <br>Block #{{$dbBlock->name}}- <textarea
                        data-nameblock="{{$dbBlock->name}}"
                        class="block_id_{{$dbBlock->name}} htmkblock">{{$dataBlockPage->html_value}}</textarea>
                @endif
            @endforeach
        @endif
    </div>


    <button id="pageSave">Соxранить изменения</button>
</div>

