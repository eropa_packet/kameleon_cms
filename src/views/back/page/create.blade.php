@extends('myviews::back.app')


@section('content')





    <div class="title_div">
        Добавить новую страницу
    </div>

    <div class="hleb_krohka">
        <a href="{{ route('back.crud.index',["param" => "home"]) }}">Главная страница</a> /
        <a href="{{ route('back.crud.index',["param" => "page"]) }}">Страницы</a>  /
        Новая запись
    </div>

   <div >
       <form  class="crud_form" action="{{ route('back.crud.store') }}" method="post">
           <div class="row_line">
                <span>Название</span>
                <input class="form_input" name="name" >
           </div>
           <div class="row_line">
               <span>Комментарий</span>
               <input class="form_input" name="comment" >
           </div>
           <div class="row_line">
               <input type="submit" class="form_submit" value="Создать">
           </div>
           @csrf
           <input type="hidden" name="param" value="page">
       </form>
   </div>
@endsection

@section('myjs')


@endsection

