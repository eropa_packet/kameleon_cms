@extends('myviews::back.app')


@section('content')

    <div class="title_div">
       Редактировать блок
    </div>

    <div class="hleb_krohka">
        <a href="{{ route('back.crud.index',["param" => "home"]) }}">Главная страница</a> /
        <a href="{{ route('back.crud.index',["param" => "block"]) }}">Блоки на странице</a>  /
        Редактировать запись
    </div>

   <div >
       <form  class="crud_form" action="{{ route('back.crud.update') }}" method="post">
           <div class="row_line">
                <span>Полное название</span>
                <input class="form_input" name="name" value="{{$data->name }}" >
           </div>
           <div class="row_line">
               <span>Полное название</span>
               <input class="form_input" name="comment" value="{{$data->comment }}">
           </div>
           <div class="row_line">
               <input type="submit" class="form_submit" value="Создать">
           </div>
           <input type="hidden" name="id_record" value="{{$data->id}}">
           <input type="hidden" name="param" value="block">
           @csrf
       </form>
   </div>
@endsection

@section('myjs')


@endsection

