<?php

use Illuminate\Support\Facades\Route;
use Eropadev\Content\Controllers\CrudController;

/**
 * Маршруты админки
 */

Route::middleware(['auth.basic'])->group(function () {
    Route::prefix('back')->group(function () {
        Route::get('/', [CrudController::class, 'index'] )->name('back.crud.index');
        Route::get('/{param}', [CrudController::class, 'index'] )->name('back.crud.index');
        Route::get('/{param}/create', [CrudController::class, 'create'] )->name('back.crud.create');
        Route::post('/crud/store', [CrudController::class, 'store'] )->name('back.crud.store');
        Route::get('/{param}/delete/{id}', [CrudController::class, 'delete'] )->name('back.crud.delete');
        Route::get('/{param}/edit/{id}', [CrudController::class, 'edit'] )->name('back.crud.edit');
        Route::post('/crud/update', [CrudController::class, 'update'] )->name('back.crud.update');
        Route::post('/crud/ajax/page', [CrudController::class, 'ajax_page'] )->name('back.crud.ajax_page');
        Route::post('/crud/ajax/pageupdate', [CrudController::class, 'ajax_pageupdate'] )->name('back.crud.ajax_pageupdate');
        Route::post('ckeditor/image_upload', [CrudController::class, 'upload'])->name('uploadimage');
    });
});

