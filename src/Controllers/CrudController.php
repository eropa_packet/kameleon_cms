<?php

namespace Eropadev\Content\Controllers;

use Eropadev\Content\Models\Block;
use Eropadev\Content\Models\Lang;
use Eropadev\Content\Models\PageLang;
use Eropadev\Content\Models\PageLangBlock;
use Eropadev\Content\Services\Crud;
use Eropadev\Content\Services\Fabrica;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CrudController extends Controller
{

    /**
     * Выводим список
     * @param $param
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index($param=""){
        if($param=="" || $param=="home")
            return view('myviews::back.home.index');
        $fabrica=new Fabrica();
        $datas=$fabrica->createFabrica($param);
        $langService=new \Eropadev\Content\Services\Lang();
        $langs=$langService->selectAll();
        return view('myviews::back.'.$param.'.index',
            ["datas" => $datas->selectAll(),'langs'=>$langs]);
    }

    /**
     * Форма на создание записи
     * @param $param
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create($param){
        $langService=new \Eropadev\Content\Services\Lang();
        $langs=$langService->selectAll();
        return view('myviews::back.'.$param.'.create',['langs'=>$langs]);
    }

    /**
     * Добавление записи
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request){
        $fabrica=new Fabrica();
        $data=$fabrica->createFabrica($request->input('param'));
        if($request->input('param')=="blogpage"){
            $data->storeFilePara($request);
        }else{
            $data->store($request->all());
        }

        return redirect()->route('back.crud.index',["param" =>$request->input('param')]);
    }

    /**
     * Удалить страницу
     * @param $param
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function delete($param,$id){
        $fabrica=new Fabrica();
        $data=$fabrica->createFabrica($param);
        $data->destroy($id);
        return view('myviews::back.'.$param.'.index',["datas" => $data->selectAll()]);
    }

    /**
     * Редактировать запись
     * @param $param
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($param,$id){
        $fabrica=new Fabrica();
        $data=$fabrica->createFabrica($param);
        $langService=new \Eropadev\Content\Services\Lang();
        $langs=$langService->selectAll();
        return view('myviews::back.'.$param.'.edit',["data" => $data->selectID($id),'langs'=>$langs]);
    }

    /**
     * Обновить запись
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request){

        $fabrica=new Fabrica();
        $data=$fabrica->createFabrica($request->input('param'));
        if($request->input('param')=="blogpage"){
            $data->updateFile($request);
        }else{
            $data->update($request->all());
        }
        return redirect()->route('back.crud.index',["param" =>$request->input('param')]);
    }

    /**
     * Получить всю информацию по странице
     * @param Request $request
     */
    public function ajax_page(Request $request){
        $lang_id=$request->input('lang_id');
        $page_id=$request->input('page_id');
        $dataLangPage=PageLang::where('lang_id',$lang_id)->where('page_id',$page_id)->first();
        if(is_null($dataLangPage)){
            $model=new PageLang();
            $model->page_id=$page_id;
            $model->lang_id=$lang_id;
            $model->slug="page_".time();
            $model->metatitle="-";
            $model->metadescriptor="-";
            $model->html_value="-";
            $model->save();
            $dataLangPage=$model;
        }
       // $dataLangPage=PageLang::where('lang_id',$lang_id)->where('page_id',$page_id)->first();
        $html=view('myviews::back.page.ajax_page_data',["dataLangPage" =>$dataLangPage])->render();
        return $html;
    }

    /**
     * Обновить данные страницы
     * @param Request $request
     * @return int
     */
    public  function  ajax_pageupdate(Request $request){
        $data=$request->all();
        $model=PageLang::where('page_id',$data['id_page'])
            ->where('lang_id',$data['lang_id'])
            ->first();
        if(is_null($model))
            $model=new PageLang();

        $model->page_id=$data['id_page'];
        $model->slug=$data['formSlug'];
        $model->lang_id=$data['lang_id'];
        $model->metatitle=$data['formMetatitle'];
        $model->metadescriptor=$data['formMetadescriptor'];
        $model->html_value=$data['formHtmlValue'];
        $model->save();

        $idPageLang=$model->id;

        PageLangBlock::where('page_lang_id',$idPageLang)->delete();
        foreach ($data['block'] as $block ){
            if($block['htmlvalue']!=""){
                // $dataBloc=$block;
                $dataBlocDB=Block::where('name',$block['nameblock'])->first();
                if(!is_null($dataBlocDB)){
                    $idbloc=$dataBlocDB->id;
                    $modelPageBlock=new PageLangBlock();
                    $modelPageBlock->page_lang_id=$idPageLang;
                    $modelPageBlock->block_id=$idbloc;
                    $modelPageBlock->html_value=$block['htmlvalue'];
                    $modelPageBlock->save();
                }
            }
        }
        return 1;
    }



    public function upload(Request $request){
        if($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;

            $request->file('upload')->move(public_path('images'), $fileName);

            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('images/'.$fileName);
            $msg = 'Image uploaded successfully';
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";

            @header('Content-type: text/html; charset=utf-8');
            echo $response;
        }
    }



}
