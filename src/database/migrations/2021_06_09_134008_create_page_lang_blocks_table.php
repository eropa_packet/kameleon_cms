<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePageLangBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_lang_blocks', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('page_lang_id');
            $table->index('page_lang_id');
            $table->bigInteger('block_id');
            $table->index('block_id');
            $table->longText('html_value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_lang_blocks');
    }
}
