<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogpagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogpages', function (Blueprint $table) {
            $table->id();
            $table->text('img_pc');
            $table->text('img_mobi');
            $table->text('ogimage');
            $table->text('ogtitle');
            $table->text('ogdescriptor');
            $table->string('title');
            $table->dateTime('datepublic');
            $table->string('slug')->unique();
            $table->text('smtext');
            $table->text('fulltext');
            $table->bigInteger('lang_id');
            $table->index('lang_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogpages');
    }
}
