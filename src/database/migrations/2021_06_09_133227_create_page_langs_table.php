<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePageLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_langs', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('page_id');
            $table->index('page_id');
            $table->string('slug');
            $table->index('slug');
            $table->bigInteger('lang_id');
            $table->index('lang_id');

            $table->text('metatitle');
            $table->text('metadescriptor');

            $table->longText('html_value');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_langs');
    }
}
