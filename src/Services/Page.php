<?php


namespace Eropadev\Content\Services;


use Illuminate\Support\Facades\DB;

class Page implements CrudInterface
{
    /**
     * Название таблиы
     * @var string
     */
    protected $table="page_heads";
    /**
     * Список полей
     * @var array
     */
    protected $collumn=array("id","name","comment");

    /**
     * Получаем все записи
     * @return \Illuminate\Support\Collection
     */
    public function selectAll()
    {
        $data=DB::table($this->table)->select($this->collumn)->get();
        return $data;
    }

    /**
     * Добовляем новую запись
     * @param array $arraRequest
     */
    public function store($arraRequest=array())
    {
        $arrInsert=array();
        foreach ($arraRequest as $key => $value) {
            if(in_array($key,$this->collumn)){
                $arrInsert[$key]=$value;
            }
        }
        DB::table($this->table)->insert($arrInsert);
    }

    /**
     * Удоляем запись
     * @param $id
     */
    public function destroy($id)
    {
        DB::table($this->table)->where('id', '=', $id)->delete();
    }

    /**
     * Получаем запись пономеру
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object|null
     */
    public function selectID($id)
    {
        $data=DB::table($this->table)->select($this->collumn)->where('id','=',$id)->first();
        if(is_null($data))
            abort(404);
        return $data;
    }

    public function update($arraRequest=array())
    {
        $id=$arraRequest["id_record"];
        $arrInsert=array();
        foreach ($arraRequest as $key => $value) {
            if(in_array($key,$this->collumn)){
                $arrInsert[$key]=$value;
            }
        }
        DB::table($this->table)
            ->where('id',$id)
            ->update($arrInsert);
    }




}
