<?php


namespace Eropadev\Content\Services;


interface CrudInterface
{
    public function selectAll();
    public function store($arraRequest=array());
    public function destroy($id);
    public function selectID($id);
}
