<?php


namespace Eropadev\Content\Services;




class Fabrica
{
    public function createFabrica($name){
        switch ($name) {
            case "lang":
                return new Lang();
            case "block":
                return new Block();
            case "page":
                return new Page();
            case "blogpage":
                return new Blogpage();
            default:
                abort(404);
        }

    }
}
