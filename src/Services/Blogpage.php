<?php


namespace Eropadev\Content\Services;


use Illuminate\Support\Facades\DB;

class Blogpage implements CrudInterface
{
    /**
     * Название таблиы
     * @var string
     */
    protected $table="blogpages";
    /**
     * Список полей
     * @var array
     */
    protected $collumn=array(
        "id",
        "img_pc",
        "img_mobi",
        "ogimage",
        "ogtitle",
        "ogdescriptor",
        "showimage",
        "title",
        "datepublic",
        "share",
        "look",
        "slug",
        "smtext",
        "fulltext",
        "lang_id");

    /**
     * Получаем все записи
     * @return \Illuminate\Support\Collection
     */
    public function selectAll()
    {
        $data=DB::table($this->table)->select($this->collumn)->get();
        return $data;
    }

    /**
     * Добовляем новую запись
     * @param array $arraRequest
     */
    public function store($arraRequest=array())
    {
        $arrInsert=array();
        foreach ($arraRequest as $key => $value) {
            if(in_array($key,$this->collumn)){
                $arrInsert[$key]=$value;
            }
        }
        DB::table($this->table)->insert($arrInsert);
    }

    public function storeFilePara($request)
    {

        $arrayImage=array();
        //в массив который всталям значение
        $arrInsert=array();

        $slugPost=$request->input('slug');

        if($slugPost==""){
            $tempName=$request->input('title');
            $slugGet=$this->slugify($tempName);
            $data=DB::table($this->table)
                ->where('slug',$slugGet)
                ->first();
            if(!is_null($data)){
                $slugGet=$slugGet."-".time();
            }
        }else{
            $slugGet=$slugPost;
            $data=DB::table($this->table)
                ->where('slug',$slugGet)
                ->first();
            if(!is_null($data)){
                $slugGet=$slugGet."-".time();
            }
        }

       // dd($slugGet);

        // Загруаем файл
        $files = $request->file();
        if (count($files) > 0) {
            foreach ($files as  $key => $file) {
                $arrayImage[]=$key;
                /* get File Extension */
                $extension = $file->getClientOriginalExtension();
                /* Your File Destination */
                $destination = $key.'/';
                /* unique Name for file */
                $filename = time()."".$key. '.' . $extension;
                /* finally move file to your destination */
                $file->move($destination, $filename);
                // записываем имя файла
                $arrInsert[$key]=$destination.$filename;
            }
        }
        foreach ($request->all() as $key => $value) {
            if(!in_array($key,$arrayImage)) {
                  if(in_array($key,$this->collumn)){
                      if($key=="slug"){
                          $arrInsert['slug']=$slugGet;
                      }else{
                          $arrInsert[$key]=$value;
                      }
                  }
            }
        }


        DB::table($this->table)->insert($arrInsert);
    }

    /**
     * Удоляем запись
     * @param $id
     */
    public function destroy($id)
    {
        DB::table($this->table)->where('id', '=', $id)->delete();
    }

    /**
     * Получаем запись пономеру
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object|null
     */
    public function selectID($id)
    {
        $data=DB::table($this->table)->select($this->collumn)->where('id','=',$id)->first();
        if(is_null($data))
            abort(404);
        return $data;
    }

    public function update($arraRequest=array())
    {


        $id=$arraRequest["id_record"];
        $arrInsert=array();
        foreach ($arraRequest as $key => $value) {
            if(in_array($key,$this->collumn)){
                $arrInsert[$key]=$value;
            }
        }
        DB::table($this->table)
            ->where('id',$id)
            ->update($arrInsert);
    }

    public function updateFile($request)
    {
        $arrayImage=array();
        //в массив который всталям значение
        $arrInsert=array();
        // Загруаем файл

        $slugPost=$request->input('slug');

        if($slugPost==""){
            $tempName=$request->input('title');
            $slugGet=$this->slugify($tempName);
            $data=DB::table($this->table)
                ->where('slug',$slugGet)
                ->where('id',"!=",$request->input("id_record"))
                ->first();
            if(!is_null($data)){
                $slugGet=$slugGet."-".time();
            }
        }else{
            $slugGet=$slugPost;
            $data=DB::table($this->table)
                ->where('slug',$slugGet)
                ->where('id',"!=",$request->input("id_record"))
                ->first();
            if(!is_null($data)){
                $slugGet=$slugGet."-".time();
            }
        }


        $files = $request->file();
        if (count($files) > 0) {
            foreach ($files as  $key => $file) {
                $arrayImage[]=$key;
                /* get File Extension */
                $extension = $file->getClientOriginalExtension();
                /* Your File Destination */
                $destination = $key.'/';
                /* unique Name for file */
                $filename = time()."".$key. '.' . $extension;
                /* finally move file to your destination */
                $file->move($destination, $filename);
                // записываем имя файла
                $arrInsert[$key]=$destination.$filename;
            }
        }
        foreach ($request->all() as $key => $value) {
            if(!in_array($key,$arrayImage)) {
                if(in_array($key,$this->collumn)){
                    if($key=="slug"){
                        $arrInsert['slug']=$slugGet;
                    }else{
                        $arrInsert[$key]=$value;
                    }

                }
            }
        }
        $id=$request->input("id_record");
        DB::table($this->table)
            ->where('id',$id)
            ->update($arrInsert);
    }



    public function getPage($listPage=0){
        $data=DB::table($this->table)
            ->select($this->collumn)
            ->get();
        return $data;
    }


    public function getPageId($id){
        $data=DB::table($this->table)
            ->select($this->collumn)
            ->where('id',$id)
            ->first();
        if(is_null($data))
            abort(404);
        return $data;
    }

    public function getPageSlug($slug){
        $data=DB::table($this->table)
            ->select($this->collumn)
            ->where('slug',$slug)
            ->first();
        if(is_null($data))
            abort(404);
        return $data;
    }


    private function slugify ($text) {

        $replace = [
            '&lt;' => '', '&gt;' => '', '&#039;' => '', '&amp;' => '',
            '&quot;' => '', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä'=> 'Ae',
            '&Auml;' => 'A', 'Å' => 'A', 'Ā' => 'A', 'Ą' => 'A', 'Ă' => 'A', 'Æ' => 'Ae',
            'Ç' => 'C', 'Ć' => 'C', 'Č' => 'C', 'Ĉ' => 'C', 'Ċ' => 'C', 'Ď' => 'D', 'Đ' => 'D',
            'Ð' => 'D', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ē' => 'E',
            'Ę' => 'E', 'Ě' => 'E', 'Ĕ' => 'E', 'Ė' => 'E', 'Ĝ' => 'G', 'Ğ' => 'G',
            'Ġ' => 'G', 'Ģ' => 'G', 'Ĥ' => 'H', 'Ħ' => 'H', 'Ì' => 'I', 'Í' => 'I',
            'Î' => 'I', 'Ï' => 'I', 'Ī' => 'I', 'Ĩ' => 'I', 'Ĭ' => 'I', 'Į' => 'I',
            'İ' => 'I', 'Ĳ' => 'IJ', 'Ĵ' => 'J', 'Ķ' => 'K', 'Ł' => 'K', 'Ľ' => 'K',
            'Ĺ' => 'K', 'Ļ' => 'K', 'Ŀ' => 'K', 'Ñ' => 'N', 'Ń' => 'N', 'Ň' => 'N',
            'Ņ' => 'N', 'Ŋ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O',
            'Ö' => 'Oe', '&Ouml;' => 'Oe', 'Ø' => 'O', 'Ō' => 'O', 'Ő' => 'O', 'Ŏ' => 'O',
            'Œ' => 'OE', 'Ŕ' => 'R', 'Ř' => 'R', 'Ŗ' => 'R', 'Ś' => 'S', 'Š' => 'S',
            'Ş' => 'S', 'Ŝ' => 'S', 'Ș' => 'S', 'Ť' => 'T', 'Ţ' => 'T', 'Ŧ' => 'T',
            'Ț' => 'T', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'Ue', 'Ū' => 'U',
            '&Uuml;' => 'Ue', 'Ů' => 'U', 'Ű' => 'U', 'Ŭ' => 'U', 'Ũ' => 'U', 'Ų' => 'U',
            'Ŵ' => 'W', 'Ý' => 'Y', 'Ŷ' => 'Y', 'Ÿ' => 'Y', 'Ź' => 'Z', 'Ž' => 'Z',
            'Ż' => 'Z', 'Þ' => 'T', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a',
            'ä' => 'ae', '&auml;' => 'ae', 'å' => 'a', 'ā' => 'a', 'ą' => 'a', 'ă' => 'a',
            'æ' => 'ae', 'ç' => 'c', 'ć' => 'c', 'č' => 'c', 'ĉ' => 'c', 'ċ' => 'c',
            'ď' => 'd', 'đ' => 'd', 'ð' => 'd', 'è' => 'e', 'é' => 'e', 'ê' => 'e',
            'ë' => 'e', 'ē' => 'e', 'ę' => 'e', 'ě' => 'e', 'ĕ' => 'e', 'ė' => 'e',
            'ƒ' => 'f', 'ĝ' => 'g', 'ğ' => 'g', 'ġ' => 'g', 'ģ' => 'g', 'ĥ' => 'h',
            'ħ' => 'h', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ī' => 'i',
            'ĩ' => 'i', 'ĭ' => 'i', 'į' => 'i', 'ı' => 'i', 'ĳ' => 'ij', 'ĵ' => 'j',
            'ķ' => 'k', 'ĸ' => 'k', 'ł' => 'l', 'ľ' => 'l', 'ĺ' => 'l', 'ļ' => 'l',
            'ŀ' => 'l', 'ñ' => 'n', 'ń' => 'n', 'ň' => 'n', 'ņ' => 'n', 'ŉ' => 'n',
            'ŋ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'oe',
            '&ouml;' => 'oe', 'ø' => 'o', 'ō' => 'o', 'ő' => 'o', 'ŏ' => 'o', 'œ' => 'oe',
            'ŕ' => 'r', 'ř' => 'r', 'ŗ' => 'r', 'š' => 's', 'ù' => 'u', 'ú' => 'u',
            'û' => 'u', 'ü' => 'ue', 'ū' => 'u', '&uuml;' => 'ue', 'ů' => 'u', 'ű' => 'u',
            'ŭ' => 'u', 'ũ' => 'u', 'ų' => 'u', 'ŵ' => 'w', 'ý' => 'y', 'ÿ' => 'y',
            'ŷ' => 'y', 'ž' => 'z', 'ż' => 'z', 'ź' => 'z', 'þ' => 't', 'ß' => 'ss',
            'ſ' => 'ss', 'ый' => 'iy', 'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G',
            'Д' => 'D', 'Е' => 'E', 'Ё' => 'YO', 'Ж' => 'ZH', 'З' => 'Z', 'И' => 'I',
            'Й' => 'Y', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',
            'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F',
            'Х' => 'H', 'Ц' => 'C', 'Ч' => 'CH', 'Ш' => 'SH', 'Щ' => 'SCH', 'Ъ' => '',
            'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'YU', 'Я' => 'YA', 'а' => 'a',
            'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo',
            'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l',
            'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's',
            'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch',
            'ш' => 'sh', 'щ' => 'sch', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e',
            'ю' => 'yu', 'я' => 'ya'
        ];

        // make a human readable string
        $text = strtr($text, $replace);

        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d.]+~u', '-', $text);

        // trim
        $text = trim($text, '-');

        // remove unwanted characters
        $text = preg_replace('~[^-\w.]+~', '', $text);

        $text = strtolower($text);

        return $text;
    }


}
