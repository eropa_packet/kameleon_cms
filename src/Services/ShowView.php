<?php


namespace Eropadev\Content\Services;


class ShowView
{
    protected $arParam=array();

    public function __construct($strParam)
    {
        $this->arParam=$strParam;
    }

    public function getHml()
    {
        return view('eropadev.'.$this->arParam->view)->render();
    }

}
